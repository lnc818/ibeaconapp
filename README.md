## iBeacon Application
This Application includes both server and client part as below:

Server: ibeacon-nodejs-server
Client: ibeacon-nodejs-client

By running both of the application, user can have a full setup to keep track where are your ibeacons and the delivery status with details and last seen.

#Hardware Requirement
In this application i am using Raspberry Pi Zero with Linux OS, but most devices that support bluetooth can also using this application

#Software Requirement
1. Nodejs
2. mongoDB

#Server
The server application act as a central point that gathering all the scanners' ibeacons information and exchange data between scanners.

It also host a web server which provide a UI for users to check the iBeacons location, and the deliver timelines

#Client
The client app will act as a scanner which will frequently scan all the iBeacons that surround it and updates the server. You may set the detecting distance, and the scanner id and name in the config.
