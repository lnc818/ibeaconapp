var http = require('http').createServer(handler); //require http server, and create server with function handler()
var fs = require('fs'); //require filesystem module
var io = require('socket.io')(http) //require socket.io module and pass the http object (server)
const axios = require('axios');
const config = require('../config.js')

http.listen(8080); //listen to port 8080
console.log("Web Server started at port 8080");

let iBeacons = [];
let iBeacons_previous = [];

function handler (req, res) { //create server
  fs.readFile(__dirname + '/public/index.html', function(err, data) { //read file index.html in public folder
    if (err) {
      res.writeHead(404, {'Content-Type': 'text/html'}); //display 404 on error
      return res.end("404 Not Found");
    } 
    res.writeHead(200, {'Content-Type': 'text/html'}); //write HTML
    res.write(data); //write data from index.html
    return res.end();
  });
}

io.sockets.on('connection', function (socket) {// WebSocket Connection
  var lightvalue = 0; //static variable for current status

  
  socket.on('light', function(data) { //get light switch status from client
    lightvalue = data;
    if (lightvalue) {
      console.log(lightvalue); //turn LED on or off, for now we will just show it in console.log
    }
  });
});


const Noble = require("noble");
const BeaconScanner = require("node-beacon-scanner");
var scanner = new BeaconScanner();

setInterval(function(){ 
    Array.prototype.diff = function(a) {
        return this.filter(function(i) {return a.indexOf(i) < 0;});
    };
    Array.prototype.getIDList = function(a) {
        return this.map(b =>{ 
            return b.id;
         });
    };
    var iBeacons_leave = (iBeacons_previous.getIDList()).diff(iBeacons.getIDList());
    var iBeacons_join = (iBeacons.getIDList()).diff(iBeacons_previous.getIDList());

    const _config = config.getConfig();
    axios.post('http://192.168.1.200:8081/beacons/update', {
      beacons: iBeacons,
      station: _config.station,
      station_type: _config.station_type,
      station_dept: _config.station_dept
    })
    .then(function (response) {
      console.log('updated!');
    })
    .catch(function (error) {
      console.log("Server offline!")
      console.log(error);
    });

    io.sockets.emit('iBeaconsUpdate', {iBeacons: iBeacons, iBeacons_leave: iBeacons_leave, iBeacons_join: iBeacons_join});

    console.log('---------- iBeacons refresh ----------');
    console.log(JSON.stringify(iBeacons, null,' '));

    iBeacons_previous = iBeacons;
    iBeacons = [];
 }, 1500);

scanner.onadvertisement = (advertisement) => {
    var beacon = {};
    ({id: beacon.id, rssi: beacon.rssi, iBeacon:{txPower: beacon.txPower}} = advertisement);
    beacon.dbm = (256 - beacon.txPower) * -1;
    beacon.distance = calculateAccuracy(beacon.dbm, beacon.rssi);
    iBeaconsHandler(beacon);
};

scanner.startScan().then(() => {
    console.log("Scanning for BLE devices...")  ;
}).catch((error) => {
    console.error(error);
});
function calculateAccuracy(dbm, rssi) {
    if (rssi == 0) {
      return -1.0; // if we cannot determine accuracy, return -1.
    }
  
    var ratio = rssi*1.0/dbm;
    if (ratio < 1.0) {
      return Math.pow(ratio,10);
    }
    else {
      var accuracy =  (0.89976)*Math.pow(ratio,7.7095) + 0.111;    
      return accuracy;
    }
  }   
function iBeaconsHandler(beacon){
  if(beacon.rssi>=-50)
  {
    var b = iBeacons.find(b => b.id === beacon.id);
    if(b == null)
    {
        iBeacons.push(beacon);
    }
    else if(b.rssi < beacon.rssi)
    {
        iBeacons.map(b =>{ 
            if(b.id === beacon.id)
                return beacon;
            else 
            return b;
         });
    }
  }
}